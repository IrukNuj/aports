# Contributor: omni <omni+alpine@hack.org>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=py3-sqlglot
pkgver=23.16.0
pkgrel=0
pkgdesc="Python SQL Parser and Transpiler"
url="https://github.com/tobymao/sqlglot"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-gpep517 py3-setuptools_scm py3-wheel"
options="!check"
# tests are included in the github release tarball but seem to require
# additional aports to be added, see requirements.txt
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/s/sqlglot/sqlglot-$pkgver.tar.gz"
builddir="$srcdir/sqlglot-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
33a0b9234ba95d5372b4673dcd877fad648e78d367ed715c8e40900fb10eea03a4bd129e10bd20f2701d504279d1a2a82dbc8256363d348d4019c3ff43927a26  sqlglot-23.16.0.tar.gz
"
